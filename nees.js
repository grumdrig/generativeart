// After: Schotter (Gravel)  - Georg Nees ,1968

function render(time) {
	ctx.clearRect(0, 0, W, H);

	let margin = 80;
	let S = 40.3;
	let T = 1.1;
	let Ncols = min(round((W - margin - margin) / S), 12);
	let Nrows = round((H - margin - margin) / S);
	if (Nrows < 2) { Nrows = 2; }

	function rand(x, y, z) {
		return fract(sin(x * 93.8823 + y * 34.23112 + z * 298.9932) * 83748.98384) - 0.5;
	}

	let p = fract(time/T);
	let v = 255 * (1 - Math.pow(p, 3));
	ctx.strokeStyle = `rgb(${v},${v},${v})`;
	let y = H / 2 - S * Nrows / 2;
	for (let col = 0; col < Ncols; ++col) {
		ctx.save();
		let x = W / 2 - S * (Ncols - 1) / 2 + S * col;
		ctx.translate(x, y);
		ctx.strokeRect(-S/2, -S/2, S, S);
		ctx.restore();
	}

	ctx.strokeStyle = "#000";
	for (let row = 0; row < Nrows; ++row) {
		let n = floor(time/T) - row;
		let y = H / 2 - S * Nrows / 2 + S * row;

		let drop = 8 * p * p;
		if (row < Nrows - 1) drop = min(drop, 1);
		y += S * drop;

		let R = (row + drop) / (Nrows - 1);
		// R *= R;
		for (let col = 0; col < Ncols; ++col) {
			let x = W / 2 - S * (Ncols - 1) / 2 + S * col;
			let r = R * S * 1;
			ctx.save();
			ctx.translate(x + rand(n, col, 0) * r, y + rand(n, col, 1) * r);
			ctx.rotate(R * rand(n, col, 3) * 1);
			ctx.strokeRect(-S/2, -S/2, S, S);
			ctx.restore();
		}
	}
}